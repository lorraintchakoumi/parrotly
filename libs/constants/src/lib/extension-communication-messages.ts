const EXTENSION_MESSAGES = {
  ADD_WORD_TO_REPETITION_LIST: 'add_word_to_selection_list',
  SHOW_ADD_WORD_TO_SELECTION_LIST: 'show_add_word_to_selection_list',
  ON_AUTH_CREDENTIALS: 'on_auth_credentials',
  ON_SIGN_OUT: 'on_sign_out',
  TRIGGER_SHOW_WORD:'trigger_show_word',
  SHOW_WORD:'show_word',
  CHANGE_THEME:'change_theme',
  GET_CURRENT_THEME:'get_current_theme',
  SHOW_SIDE_NAV: 'show_side_nav',
  PLAY_TEXT: 'play_text',
  TRANSLATE_TEXT: 'translate_text',
  TRANSLATION_COMPLETE: 'translation_complete',
  KNOW_WORD: 'know_word',
  UPDATE_USER_SETTINGS:'update_user_settings',
} as const

export {
  EXTENSION_MESSAGES
}
