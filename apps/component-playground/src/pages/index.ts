import ExtensionSideNavPage from './ExtensionSideNav.page.svelte'
import WelcomePage from './Welcome.page.svelte'
import ShowWordPage from './ShowWord.page.svelte'
import AddToRepetitionList from './AddToRepetitionList.page.svelte'

export {
  ExtensionSideNavPage,
  WelcomePage,
  ShowWordPage,
  AddToRepetitionList,
}
